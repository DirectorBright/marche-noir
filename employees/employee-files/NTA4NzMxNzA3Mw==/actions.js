function encodeToBase64() {
    let string = document.getElementById('base64encode').value;
    let encode = btoa(string);
    document.getElementById('encodeOutput').innerHTML = encode;
}

function decodeToBase64() {
    let string2 = document.getElementById('base64decode').value;
    let decode2 = atob(string2);
    document.getElementById('decodeOutput').innerHTML = decode2;
}

async function changeAccount() {
    const response = await fetch("https://marche-noir.vercel.app/employees/employee-files/bGl0ZXJhbGx5IGFsbCBvZiB0aGUgc3RhZmYncyBQSU5z.json");
    const accounts = await response.json();
    console.log(accounts);
    let jsonData = accounts;
    let raw = jsonData[document.getElementById('PIN').value];
    console.log(raw);
    let newURL = btoa(raw);
    window.location.replace('./' + newURL)
}
